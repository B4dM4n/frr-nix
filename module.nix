{ self }:
{ config, pkgs, lib, ... }:

with lib;
let
  cfg = config.services.frr;

  yesno = b: if b then "yes" else "no";

  concatMapAttrs = f: attrs: concatStrings (mapAttrsToList f attrs);

  mkDaemonOptions = name: { defaultOptions, canEnable ? true }:
    with types;
    mkOption {
      type = submodule ({ name, config, ... }: {
        options = optionalAttrs canEnable
          {
            enable = mkEnableOption "${name} daemon";
          } // {
          options = mkOption {
            default = defaultOptions;
            type = str;
          };
        };
      });

      default = { };
    };

  defaultDaemonOptions = {
    watchfrr = { defaultOptions = ""; canEnable = false; };
    zebra = { defaultOptions = "  -A 127.0.0.1 -s 90000000"; canEnable = false; };
    bgpd = { defaultOptions = "   -A 127.0.0.1"; };
    ospfd = { defaultOptions = "  -A 127.0.0.1"; };
    ospf6d = { defaultOptions = " -A ::1"; };
    ripd = { defaultOptions = "   -A 127.0.0.1"; };
    ripngd = { defaultOptions = " -A ::1"; };
    isisd = { defaultOptions = "  -A 127.0.0.1"; };
    pimd = { defaultOptions = "   -A 127.0.0.1"; };
    ldpd = { defaultOptions = "   -A 127.0.0.1"; };
    nhrpd = { defaultOptions = "  -A 127.0.0.1"; };
    eigrpd = { defaultOptions = " -A 127.0.0.1"; };
    babeld = { defaultOptions = " -A 127.0.0.1"; };
    sharpd = { defaultOptions = " -A 127.0.0.1"; };
    pbrd = { defaultOptions = "   -A 127.0.0.1"; };
    staticd = { defaultOptions = "-A 127.0.0.1"; };
    bfdd = { defaultOptions = "   -A 127.0.0.1"; };
    fabricd = { defaultOptions = "-A 127.0.0.1"; };
    vrrpd = { defaultOptions = "  -A 127.0.0.1"; };
    pathd = { defaultOptions = "  -A 127.0.0.1"; };
  };

  daemonNames = attrNames defaultDaemonOptions;
in
{
  options = {
    services.frr = with types; {
      enable = mkEnableOption "frr";

      package = mkOption {
        default = self.packages.${pkgs.stdenv.hostPlatform.system}.frr;
        defaultText = "self.packages.\${pkgs.stdenv.hostPlatform.system}.frr";
        description = "frr package to use.";
        type = package;
      };

      user = mkOption {
        type = str;
        default = "frr";
        description = "User the frr daemon should execute under.";
      };

      group = mkOption {
        type = str;
        default = "frr";
        description = "Group the frr daemon should execute under.";
      };

      config = mkOption {
        type = types.lines;
        default = "";
        description = ''
          Literal string to write to <literal>/etc/frr/frr.conf</literal>.
        '';
      };

      daemons = mkOption {
        type = submodule {
          options = {
            vtysh = mkOption {
              type = submodule ({ name, config, ... }: {
                options = {
                  enable = mkEnableOption "vtysh";
                };

                config = {
                  enable = mkDefault true;
                };
              });

              default = { };
            };

            extraConfig = mkOption {
              type = types.lines;
              default = "";
              description = ''
                Literal string to append to <literal>/etc/frr/daemons</literal>.
              '';
            };
          } // mapAttrs mkDaemonOptions defaultDaemonOptions;
        };

        default = { };
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.packages = [ cfg.package ];

    environment.systemPackages = [ cfg.package ];

    systemd.services.frr = {
      reloadIfChanged = true;
      restartTriggers = [
        config.environment.etc."frr/daemons".source
        config.environment.etc."frr/frr.conf".source
      ];
      wantedBy = [ "multi-user.target" ];
    };

    environment.etc."frr/daemons".text = concatMapAttrs
      (d: o: ''
        ${optionalString (o.canEnable or true) "${d}=${yesno cfg.daemons.${d}.enable}"}
        ${d}_options="${cfg.daemons.${d}.options}"
      '')
      defaultDaemonOptions
    + ''
      vtysh_enable=${yesno cfg.daemons.vtysh.enable}
      ${cfg.daemons.extraConfig}
    '';

    environment.etc."frr/frr.conf".text = cfg.config;

    environment.etc."frr/vtysh.conf".text = "service integrated-vtysh-config";

    users.users = optionalAttrs (cfg.user == "frr") {
      frr = {
        group = cfg.group;
        home = "/var/lib/frr";
        isSystemUser = true;
        description = "Daemon user for the frr service";
      };
    };

    users.groups = optionalAttrs (cfg.group == "frr") {
      frr = { };
    };
  };
}
