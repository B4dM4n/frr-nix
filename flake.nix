{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, ... } @ inputs:
    let
      inherit (builtins) intersectAttrs;
      inherit (inputs.nixpkgs) lib;
    in
    with lib;
    let
      systems = [
        "aarch64-linux"
        "armv7l-linux"
        "i686-linux"
        "x86_64-linux"
      ];

      forAllSystems = fn: genAttrs systems (system:
        fn (intersectAttrs (functionArgs fn) {
          inherit system;
          nixpkgs = inputs.nixpkgs.legacyPackages.${system};
        }));
    in
    {
      nixosModule = import ./module.nix { inherit self; };

      defaultPackage = forAllSystems ({ system, nixpkgs }: self.packages.${system}.frr);

      packages = forAllSystems ({ system, nixpkgs }: rec {
        frr = nixpkgs.callPackage ./pkg.nix { };
      });

      checks = forAllSystems ({ system, nixpkgs }: {
        frr = nixpkgs.nixosTest ({ pkgs, ... }: {
          name = "frr";

          machine = { ... }: {
            imports = [
              self.nixosModule
            ];
            config = {
              services.frr = {
                enable = true;
              };
            };
          };

          testScript = ''
            start_all()

            machine.wait_for_unit("frr.service")
          '';
        });
      });
    };
}
