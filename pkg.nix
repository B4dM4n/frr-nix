{ stdenv
, lib
, fetchFromGitHub
, autoreconfHook
, bison
, cmake
, cmocka
, flex
, gettext
, json_c
, libcap
, libelf
, ncurses
, pcre2
, perl
, pkg-config
, python3
, readline
, withSystemd ? true
, systemd ? null
}:

assert withSystemd -> systemd != null;

let
  libyang = stdenv.mkDerivation rec {
    pname = "libyang";
    version = "2.0.97";

    src = fetchFromGitHub {
      owner = "CESNET";
      repo = pname;
      rev = "v${version}";
      sha256 = "bfNN0pfZmKqdQlVvYiz6LgpvTJ6Q/UoVjOrNZOixg2g=";
    };

    nativeBuildInputs = [
      cmake
      pkg-config
      cmocka
    ];

    buildInputs = [
      pcre2
    ];

    doInstallCheck = true;
    installCheckTarget = "test";

    cmakeFlags = [
      "-DCMAKE_BUILD_TYPE=Release"
      "-DENABLE_TESTS=ON"
    ];
  };
in
stdenv.mkDerivation rec {
  pname = "frr";
  version = "8.0.1";

  src = fetchFromGitHub {
    owner = "FRRouting";
    repo = pname;
    rev = "frr-${version}";
    sha256 = "GTSDquc/TZgy6CFvEpRMT2AJZO6o0ILEambaFWyJ1Q0=";
  };

  postPatch = ''
    patchShebangs lib/route_types.pl lib/gitversion.pl
  '';

  configureFlags = [
    "--sysconfdir=/etc/frr"
    "--localstatedir=/run/frr"
    "--disable-pathd" # build fails with "struct.error: unpack requires a buffer of 72 bytes"
  ] ++ lib.optionals withSystemd [
    "--enable-systemd"
  ];

  nativeBuildInputs = [
    autoreconfHook
    bison
    flex
    perl
    pkg-config
    python3
  ];

  buildInputs = [
    gettext
    json_c
    libcap
    libelf
    libyang
    ncurses
    pcre2
    python3
    readline
  ] ++ lib.optionals withSystemd [
    systemd
  ];

  enableParallelBuilding = true;

  postInstall = ''
    sed -i -e '/^PATH=/d' $out/sbin/frr $out/sbin/frrcommon.sh
  '' + lib.optionalString withSystemd ''
    mkdir -p $out/lib/systemd/system
    cp tools/frr.service tools/frr@.service $out/lib/systemd/system/
  '';
}
